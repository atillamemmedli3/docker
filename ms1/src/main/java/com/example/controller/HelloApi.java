package com.example.controller;

import com.example.dto.WebclientResponse;
import com.example.service.CountService;
import com.example.service.WebclientService;
import com.example.service.WebclientServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
@RequiredArgsConstructor
public class HelloApi {
    private final CountService countService;
    private final WebclientService webclientService;

//    @GetMapping()
//    public String sayHello(){
//        return "Hello from ms1  "+ countService.getCount();
//    }

    @GetMapping()
    public WebclientResponse webClient(){
        return webclientService.testWebclient();
    }
}
