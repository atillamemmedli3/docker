package com.example.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@RestController
public class Photo {
    @PostMapping("/upload")
    public ResponseEntity<String> uploadPhoto(@RequestPart("image") MultipartFile image) {
        System.out.println("Uploaded photo: " + image.getOriginalFilename() + ", Size: " + image.getSize() + " bytes");
        try {
            String folderPath = "/Users/mac/Desktop/image/";
            String filePath = folderPath + image.getOriginalFilename();

            File folder = new File(folderPath);
            if (!folder.exists()) {
                folder.mkdirs(); // Klasör yoksa oluştur
            }

            File destFile = new File(filePath);
            image.transferTo(destFile);

            return ResponseEntity.ok("Uploaded photo: " + image.getOriginalFilename() + ", Size: " + image.getSize() + " bytes");

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to upload photo: " + e.getMessage());
        }
    }
}
