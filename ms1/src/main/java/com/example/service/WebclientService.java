package com.example.service;

import com.example.dto.WebclientResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

public interface WebclientService {
    WebclientResponse testWebclient();

}
