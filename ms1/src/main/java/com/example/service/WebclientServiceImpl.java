package com.example.service;

import com.example.dto.WebclientResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
@RequiredArgsConstructor
public class WebclientServiceImpl implements WebclientService{

    private final WebClient webClient;

    public WebclientResponse testWebclient (){
        WebclientResponse response = webClient
                .get()
                .uri("/ms2/hello")
                .retrieve()
                .bodyToMono(WebclientResponse.class)
                .block();
        System.out.println(response.response);
        return response;
    }

}
