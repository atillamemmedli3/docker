package com.example.service;

public interface CountService {
    Integer getCount();
}
