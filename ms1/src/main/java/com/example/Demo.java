package com.example;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
public class Demo {
    private String name;
    private int age;
    private String address;
    private String city;
    private String state;
    private String zip;
    private String country;
    private String phone;
}
