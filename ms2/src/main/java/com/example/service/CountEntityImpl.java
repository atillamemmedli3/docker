package com.example.service;

import com.example.model.CountEntity;
import com.example.repository.CountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CountEntityImpl implements CountService{
    private final CountRepository countRepository;
    @Override
    public Integer getCount() {
       CountEntity countEntity= countRepository.findById(1L).orElse(new CountEntity());
       int count = countEntity.getCount();
       count++;

       countEntity.setCount(count);
       countRepository.save(countEntity);
       return count;
    }
}
