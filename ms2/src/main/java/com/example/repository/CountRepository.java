package com.example.repository;

import com.example.model.CountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountRepository extends JpaRepository<CountEntity,Long> {
}
