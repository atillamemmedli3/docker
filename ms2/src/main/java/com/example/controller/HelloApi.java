package com.example.controller;

import com.example.service.CountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/localhost:9097/ms2/hello")
@RequiredArgsConstructor
public class HelloApi {
    private final CountService countService;

    @GetMapping()
    public String sayHello(){
        return "Hello from ms2  "+ countService.getCount();
    }
}

